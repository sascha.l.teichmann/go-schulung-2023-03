# Go Schulung Linuxhotel 27.11.2023 - 29.11.2023

* [Agenda](agenda.md)
* [Links](links.md)
* [Code-Beispiele](https://gitlab.com/sascha.l.teichmann/go-examples)
* [Live-Coding-Beispiele](workspaces/README.md)


