package main

import (
	"fmt"
)

func main() {
	ch1 := make(chan int)
	ch2 := make(chan int)

	go func() {
		defer close(ch1)
		for i := 0; i < 5; i++ {
			ch1 <- i
		}
		fmt.Println("1 end")
	}()

	go func() {
		defer close(ch2)
		for i := 0; i < 10; i++ {
			ch2 <- -i - 1
		}
		fmt.Println("2 end")
	}()

	var remain chan int
	for remain == nil {
		select {
		case c, ok := <-ch1:
			if ok {
				fmt.Println("ch1", c)
			} else {
				remain = ch2
			}
		case c, ok := <-ch2:
			if ok {
				fmt.Println("ch2", c)
			} else {
				remain = ch1
			}
		}
	}
	for c := range remain {
		fmt.Println("remain", c)
	}
}
