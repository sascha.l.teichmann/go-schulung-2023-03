package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type calculator float64

func (c *calculator) add(x float64) {
	*c += calculator(x)
}

func (c *calculator) sub(x float64) {
	*c -= calculator(x)
}

func (c *calculator) mul(x float64) {
	*c *= calculator(x)
}

var operators = map[string]func(*calculator, float64){
	"+": (*calculator).add,
	"-": (*calculator).sub,
	"*": (*calculator).mul,
}

func main() {

	var c calculator

	sc := bufio.NewScanner(os.Stdin)

	for sc.Scan() {
		line := sc.Text()
		prefix, suffix, ok := strings.Cut(line, " ")
		if !ok {
			continue
		}
		value, err := strconv.ParseFloat(suffix, 64)
		if err != nil {
			log.Printf("parse error: %v\n", err)
			continue
		}
		if op := operators[prefix]; op != nil {
			op(&c, value)
		}
		fmt.Println(c)
	}

	if err := sc.Err(); err != nil {
		log.Fatalf("error: %v\n", err)
	}

	fmt.Println(c)
}
