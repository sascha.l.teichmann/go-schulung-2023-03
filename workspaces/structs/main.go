package main

import (
	"fmt"
	"log"
	"os"

	"encoding/json"
)

type myStruct struct {
	Feld1 int `json:"field1"`
	feld2 float64
}

func process(st myStruct) {

	if err := json.NewEncoder(os.Stdout).Encode(st); err != nil {
		log.Printf("error: %v\n", err)
	}
}

func main() {
	st := myStruct{
		42,
		872489.09,
	}

	st.Feld1 = 32
	//st.feld2 = 3.1415
	fmt.Println(st)
	process(st)
}
