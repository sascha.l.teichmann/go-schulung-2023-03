package main

import (
	"errors"
	"log"
)

func calulate(a, b float64) (float64, error) {

	if b == 0 {
		return 0, errors.New("division by zero")
	}

	return a / b, nil
}

func main() {
	// err-lang
	if _, err := calulate(20, 10); err != nil {
		log.Printf("error: %v\n", err)
	}
	if _, err := calulate(20, 0); err != nil {
		log.Printf("error 2: %v\n", err)
	}
}
