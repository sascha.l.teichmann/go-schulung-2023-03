package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

func main() {
	//var mu sync.Mutex
	var counter int64

	var wg sync.WaitGroup

	for i := 0; i < 100; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for i := 0; i < 100_000; i++ {
				atomic.AddInt64(&counter, 1)
				//mu.Lock()
				//counter++
				//mu.Unlock()
			}
		}()
	}

	wg.Wait()
	//time.Sleep(10 * time.Second)
	fmt.Println(counter)
}
