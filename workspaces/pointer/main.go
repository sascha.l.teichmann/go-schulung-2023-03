package main

import "fmt"

func modify(x *int) {
	*x *= 3
}

func badC() *int {
	var tmp int
	return &tmp
}

func main() {
	var x int = 42
	modify(&x)
	fmt.Println(x)

	y := new(int)
	*y = 78
	fmt.Println(*y)
}
