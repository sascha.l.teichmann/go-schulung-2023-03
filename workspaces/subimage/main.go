package main

import (
	"fmt"
	"image"
	"log"
	"os"

	_ "image/png"
)

func main() {
	for _, arg := range os.Args[1:] {
		fmt.Println(arg)
		f, err := os.Open(arg)
		if err != nil {
			log.Fatalf("error: %v\n", err)
		}
		img, _, err := image.Decode(f)
		f.Close()
		if err != nil {
			log.Fatalf("error: %v\n", err)
		}

		if si, ok := img.(interface {
			SubImage(image.Rectangle) image.Image
		}); ok {
			bounds := img.Bounds()
			bounds.Min.X += 10
			bounds.Min.Y += 10
			smaller := si.SubImage(bounds)
			fmt.Printf("smaller: %v\n", smaller.Bounds())
		}
	}
}
