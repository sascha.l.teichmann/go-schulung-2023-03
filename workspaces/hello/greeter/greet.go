package greeter

import (
	"fmt"
)

func init() {
	fmt.Println("hello from init1")
}

func init() {
	fmt.Println("hello from init2")
}

func Greet() {
	fmt.Println("Hello from Greet")
	greet()
}
