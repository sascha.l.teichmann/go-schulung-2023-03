package main

import "fmt"

func extend(slice *[]int) {
	*slice = append(*slice, 1, 2, 4)
}

func main() {

	var slice []int

	fmt.Println(slice == nil)
	fmt.Println(len(slice))

	var backing [10]int

	slice = backing[1 : 3+1]

	slice[0] = 42
	slice[2] = 12
	//slice[3] = 2

	fmt.Println(backing)
	fmt.Println(slice)

	slice = slice[:len(slice)+1]
	slice[3] = 2
	fmt.Println(backing)
	fmt.Println(slice)
	fmt.Println(cap(slice))
	//slice = slice[:len(slice)+10]

	dslice := make([]int, 2, 5)
	fmt.Println(cap(dslice), len(dslice))
	fmt.Println(dslice)
	dslice = append(dslice, 789, 89, 89)
	fmt.Println(dslice)
	dslice = append(dslice, 1, 2, 3)
	fmt.Println(dslice)
	fmt.Println(cap(dslice), len(dslice))
	copy(dslice[1:], dslice)
	fmt.Println(dslice)

	extend(&dslice)
	fmt.Println(dslice)

	/*

		slice2 := slice[1:]
		slice2[0] = 78
		fmt.Println(backing)
		fmt.Println(slice2)
	*/
}
