package main

import (
	"fmt"
	"log"
	"net/http"
)

type controller struct {
	content string
}

func (c *controller) index(
	rw http.ResponseWriter,
	req *http.Request,
) {
	fmt.Fprintf(rw, "I was here: %s\n", c.content)
}

func main() {
	mux := http.NewServeMux()

	c := controller{content: "Hello, World"}

	mux.HandleFunc("/index", c.index)

	/*
		mux.HandleFunc("/index",
			func(
				rw http.ResponseWriter,
				req *http.Request,
			) {
				c.index(rw, req)
			})
	*/

	log.Fatalln(http.ListenAndServe(
		"localhost:8080",
		mux))
}
