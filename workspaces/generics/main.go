package main

import (
	"cmp"
	"fmt"
)

type tree[T any] struct {
	left  *tree[T]
	right *tree[T]
	value T
}

func (t *tree[T]) inorder(visitor func(T)) {
	if t == nil {
		return
	}
	t.left.inorder(visitor)
	visitor(t.value)
	t.right.inorder(visitor)
}

/*
type constraint interface {
	~int | string | float64 // type set
}
*/

type myInt int

func min[T cmp.Ordered](a, b T) T {
	//var c T
	if a < b {
		return a
	}
	return b
}

func main() {
	fmt.Println(min[float64](1, -3))
	fmt.Println(min("Hello", "Spencer"))
	fmt.Println(min(3.1415, 0.815))

	m := min[string]

	fmt.Println(m("Hello", "Spencer"))

	var a, b myInt = 2, 4
	fmt.Println(min(a, b))

	root := tree[int]{
		left: &tree[int]{
			value: 10,
		},
		right: &tree[int]{
			value: 20,
		},
		value: 15,
	}

	root.inorder(func(x int) {
		fmt.Println(x)
	})
}
