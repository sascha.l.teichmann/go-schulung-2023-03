package main

import (
	"io"
	"log"
	"os"
)

type tr struct {
	io.Reader
}

func (t *tr) Read(b []byte) (int, error) {
	n, err := t.Reader.Read(b)
	for i, c := range b[:n] {
		if 'a' <= c && c <= 'z' {
			b[i] = 'A' + c - 'a'
		}
	}
	return n, err
}

func main() {
	t := tr{os.Stdin}
	if _, err := io.Copy(os.Stdout, &t); err != nil {
		log.Fatalf("error: %v\n", err)
	}
}
