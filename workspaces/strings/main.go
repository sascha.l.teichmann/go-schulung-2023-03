package main

import (
	"fmt"
	"strings"
)

// here-document
var text = `
Dies ist
ein mehrzeiliger Text
.`

func main() {
	fmt.Printf(`vim-go\n`)
	fmt.Println()
	var s string = "Hello 🐈world"

	if s == ""+"Hallo" {
		fmt.Println("Leerstring")
	}

	fmt.Println(s)

	//var r rune = 'A'

	for i, r := range s {
		fmt.Printf("%d: %c\n", i, r)
	}

	fmt.Printf("%c\n", s[6]) // byte weise

	for i, r := range []rune(s) {
		fmt.Printf("%d: %c\n", i, r)
	}

	// s[2] = 'A' immutable

	s = strings.ReplaceAll(s, "🐈", "🐕")
	fmt.Println(s)
}
