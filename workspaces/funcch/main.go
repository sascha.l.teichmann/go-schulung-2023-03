package main

import (
	"log"
	"net/http"
)

func main() {
	s := newServer()

	done := make(chan struct{})

	go s.run(done)

	c := &controller{
		server: s,
	}

	auth := authorize("blablub")

	fetch := auth(http.HandlerFunc(c.fetch))

	s.store("World", "Hello")

	mux := http.NewServeMux()
	mux.Handle("/fetch", fetch)

	log.Fatalln(http.ListenAndServe("localhost:8080", mux))

	/*
		s.store("Hello", "World")
		fmt.Println(s.fetch("Hello"))
		fmt.Println(s.dump())
		s.kill()
	*/

	<-done
}
