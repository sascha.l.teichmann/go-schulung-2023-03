package main

import "testing"

func TestServerFetch(t *testing.T) {

	s := newServer()

	done := make(chan struct{})

	go s.run(done)

	s.store("key", "value")
	x := s.fetch("key")
	s.kill()

	if x != "value" {
		t.Fatalf("got %q expected 'value'", x)
	}

	<-done
}
