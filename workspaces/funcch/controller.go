package main

import (
	"fmt"
	"net/http"
)

type controller struct {
	server *server
}

func (c *controller) fetch(
	rw http.ResponseWriter,
	req *http.Request,
) {
	key := req.FormValue("key")
	fmt.Println("key", key)
	fmt.Fprintf(rw, c.server.fetch(key))

	user, ok := GetUser(req.Context())
	fmt.Println(user, ok)
}
