package main

import (
	"context"
	"net/http"
)

type authKey int

var key authKey = 6786

func GetUser(ctx context.Context) (string, bool) {
	if user := ctx.Value(key); user != nil {
		return user.(string), true
	}
	return "", false
}

func authorize(
	token string,
) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(
			func(rw http.ResponseWriter,
				req *http.Request,
			) {
				if req.Header.Get("X-Auth") != token {
					http.Error(
						rw,
						"You shall not pass!",
						http.StatusUnauthorized)
					return
				}
				ctx := context.WithValue(
					req.Context(),
					key, "Micky Maus")
				req = req.WithContext(ctx)
				next.ServeHTTP(rw, req)
			})
	}

}
