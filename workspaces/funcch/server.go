package main

import (
	"encoding/json"
	"log"
	"strings"
)

type server struct {
	//mu       sync.Mutex
	database map[string]string
	calls    chan func(*server)
	die      bool
}

func newServer() *server {
	return &server{
		database: map[string]string{},
		calls:    make(chan func(*server)),
	}
}

func (s *server) fetch(key string) string {
	// s.mu.Lock()
	// defer s.mu.Unlock()
	result := make(chan string)
	s.calls <- func(s *server) {
		result <- s.database[key]
	}
	return <-result
}

func (s *server) store(key, value string) {
	// s.mu.Lock()
	// defer s.mu.Unlock()
	s.calls <- func(s *server) {
		s.database[key] = value
	}
}

func (s *server) kill() {
	s.calls <- func(s *server) {
		s.die = true
	}
}

func (s *server) dump() string {
	result := make(chan string)
	s.calls <- func(s *server) {
		var b strings.Builder
		enc := json.NewEncoder(&b)
		if err := enc.Encode(s.database); err != nil {
			log.Printf("error: %v\n", err)
		}
		result <- b.String()
	}
	return <-result
}

func (s *server) run(done chan struct{}) {
	defer close(done)
	for call := range s.calls {
		call(s)
		if s.die {
			break
		}
	}
}
