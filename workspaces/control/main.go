package main

import "fmt"

func main() {

	//var i int
	//var i int = 0
	i := 0

	//for i < 10 { // while
	//for { // while
	for i := 0; i < 10; i++ {
		fmt.Println(i)

		if j := i * 2; j < 10 {
			fmt.Println("kleiner 5", j)
		} else if j > 8 {
			fmt.Println("größer 8", j)
			break

		} else {
			fmt.Println("größer 5", j)
		}
	}

	fmt.Println("after", i)

	for i := 0; i < 6; i++ {
		switch j := i * 2; j {
		case 0:
			fmt.Println("Null")
			fallthrough
		case 1:
			fmt.Println("eins")
		case 3, 4:
			fmt.Println("drei, vier")
		default:
			fmt.Println("sonst")
		}
	}

	switch {
	case 0 <= i && i < 10:
		fallthrough
	case 10 < i && i < 20:
	default:
	}

	/* do {
	   } while(cond);

	for {
		if cond {
			break
		}
	}
	*/

outer:
	for i := 0; i < 5; i++ {
		//inner:
		for j := 0; j < 5; j++ {
			if i == j {
				continue outer
			}
			if j == 4 {
				goto exit
			}
			fmt.Println(i, j)
		}
	}

	goto outer
exit:
}
