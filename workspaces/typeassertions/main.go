package main

import "fmt"

type myString int

func (ms myString) String() string {
	return fmt.Sprintf("[%d]", ms)
}

func printme(x any) {

	if v, ok := x.(int); ok {
		fmt.Printf("%T: %d\n", v, v)
	}

	switch v := x.(type) {
	case int:
		fmt.Printf("int -> %T: %d\n", v, v)
	case float64:
		fmt.Printf("float64 -> %T: %f\n", v, v)

	case fmt.Stringer:
		fmt.Printf("stringer: %s\n", v.String())

	default:
		fmt.Printf("default -> %T: %v\n", v, v)
	}

	//fmt.Println(x)
}

func main() {
	printme(1)
	printme(1.238479)
	printme("hELLO")

	ms := myString(42)
	printme(ms)
}
