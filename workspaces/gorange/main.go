package main

import "fmt"

func main() {

	var (
		jobs    = make(chan int)
		done    = make(chan struct{})
		results = make(chan int)
		rdone   = make(chan struct{})
	)

	go func() {
		defer close(done)
		for x := range jobs {
			results <- x * 2
		}
	}()

	go func() {
		defer close(rdone)
		for result := range results {
			fmt.Println(result)
		}
	}()

	for i := 0; i < 10; i++ {
		jobs <- i
	}
	jobs <- 1001
	close(jobs)

	<-done
	close(results)
	<-rdone
}
