package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
)

var matcher = regexp.MustCompile(`a+`)

type myErr struct {
	pos int
}

func (me *myErr) Error() string {
	return fmt.Sprintf("pos: %d", me.pos)
}

func level1() {
	level2()
}

func level2() {
	level3()
}

func level3() {
	panic(&myErr{pos: 666})
}

func API() (result int, err error) {

	defer func() {
		fmt.Println("hello from defer")
		if r := recover(); r != nil {
			if p, ok := r.(*myErr); ok {
				fmt.Println("Wir sind Schuld", p)
				err = p
			} else {
				panic(r)
			}
		}
	}()

	level1()

	return 42, nil
}

func main() {

	if _, err := API(); err != nil {
		log.Fatalf("+++error: %v\n", err)
	}

	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		line := sc.Text()
		if matcher.MatchString(line) {
			fmt.Println(line)
		}
	}

	if err := sc.Err(); err != nil {
		log.Fatalf("error: %v\n", err)
	}
}
