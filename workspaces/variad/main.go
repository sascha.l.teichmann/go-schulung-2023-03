package main

import "fmt"

func printme(greet string, args ...int) {
	for _, arg := range args {
		fmt.Println(greet, arg)
	}
}

func main() {
	printme("hallo", 2, 1)
	slice := []int{34, 56}
	printme("hallo", slice...)
}
