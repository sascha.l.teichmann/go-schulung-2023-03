package main

import "fmt"

type myInt int

type theirType int

type yourType struct {
	myInt // embedding
	*theirType
}

func (tt theirType) hello() {
	fmt.Println("hello from theirType")
}

func (mi myInt) printme() {
	fmt.Printf("myInt.printme: %d\n", mi)
}

func (tt theirType) printme() {
	fmt.Printf("theirType.printme: %d\n", tt)
}

func (yt yourType) printme() {
	yt.myInt.printme()
	yt.theirType.printme()
}

func main() {

	tt := new(theirType)

	yt := yourType{3, tt}
	yt.printme()
	yt.hello()

	fmt.Println(yt)
}
