package main

func main() {

	/*
		var x string
		var x bool
		var x complex128
		var x complex64
		var x float64
		var x float32
		var x uintptr
		var x uint64
		var x uint32
		var x uint16
		var x uint8
		var x byte // uint8
		var x int64
		var x int32 // rune
		var x int16
		var x int8
	*/
	//var x uint
	//var x int

	var x uint8  //  0 - 255
	var y uint16 // 0 - 65535

	y = uint16(x) // type conversion

	z := float32(3.1415)

	zi := int(z)

	b := 10 < 11 || 11 < 10 && 10 < 89

	_ = x
	_ = y
	_ = zi
	_ = b
}
