package main

import (
	"fmt"
)

func meineFunktion(x, y int) (w, z int) {
	//return x * y, 42
	w, z = x*y, 42
	return
}

func evalute(fn func(int, int) (int, int)) {
	result, valueZwei := fn(10, 3)
	fmt.Println(result, valueZwei)
}

func increment(x int) func(int) int {
	return func(z int) int {
		r := z + x
		x++
		return r
	}
}

func main() {
	//var x, y int
	/*
		var (
			x      int = 42
			ySuper int
		)
	*/
	//fmt.Println(meineFunktion(10, 3))
	result, value := meineFunktion(10, 3)
	fmt.Println(result, value)

	x, y := 10, 20
	x, y = y, x

	_, _ = x, y

	// var fn func(int, int) (int, int)
	// fn = meineFunktion
	fn := meineFunktion
	evalute(fn)
	evalute(meineFunktion)

	inc := increment(10)
	fmt.Println(inc(12345))
	fmt.Println(inc(12))
	fmt.Println(inc(12))
	fmt.Println(inc(12))
}
