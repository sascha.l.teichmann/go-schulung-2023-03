package main

import "fmt"

type auto int

type motor float32
type brakes float32

type driving interface {
	Fahren()
}

type braking interface {
	Bremsen()
}

type car interface {
	driving
	braking
}

func (motor) Fahren() {
	fmt.Println("motor.Fahren")
}

func (brakes) Bremsen() {
	fmt.Println("brakes.Bremsen")
}

func drive(c car) {
	c.Fahren()
	c.Bremsen()
}

func (auto) Fahren() {
	fmt.Println("auto.Fahren")
}

func (auto) Bremsen() {
	fmt.Println("auto.Bremsen")
}

func main() {
	var c car
	var a auto
	c = a
	drive(c)

	var m motor

	// structural typing
	adhoc := struct {
		//motor
		driving
		brakes
	}{driving: m}

	drive(adhoc)
}
