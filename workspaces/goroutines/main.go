package main

import (
	"fmt"
)

func main() {

	// var ch chan bool

	done := make(chan struct{})

	go func() {
		defer close(done)
		//defer func() { done <- struct{}{} }()

		fmt.Println("Hello, World")

		// close(done)
		// done <- struct{}{}
	}()

	_, ok := <-done

	fmt.Println(ok)

	//time.Sleep(time.Second)
}
