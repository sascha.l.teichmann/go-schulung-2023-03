package main

import "fmt"

const (
	a int = iota * 2
	b
	c
	d
)

func main() {

	const pi = 3.1415 // untypisiert: Default type float64

	var x int = pi * 10_000

	const piT float64 = 3.1415 // typisiert: float64

	// var y int = piT * 10_000

	_ = x

	fmt.Println(a, b, c, d)
}
