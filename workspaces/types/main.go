package main

import "fmt"

type (
	counter int
	call    func()
)

func (c counter) printme() {
	fmt.Printf("counter: %d\n", c)
}

func (c *counter) modify(x int) {
	*c = counter(x)
}

func main() {

	var c counter

	var i int = 5

	c += 3

	c *= counter(i)

	fmt.Println(c)
	c.printme()
	c.modify(42)
	//(&c).modify(42)
	c.printme()
}
