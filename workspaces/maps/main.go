package main

import "fmt"

func main() {
	//var m map[string]int

	m := map[string]int{
		"Gordon": 79,
		"Mary":   0,
	}

	fmt.Println(m == nil)
	fmt.Println(len(m))

	//m = make(map[string]int, 100)

	m["Crash"] = 666
	fmt.Println(m)
	fmt.Println(m["Mary"])
	fmt.Println(m["Gary"])

	for _, k := range []string{"Gary", "Mary"} {
		if v, ok := m[k]; ok {
			fmt.Printf("%s: %d\n", k, v)
		}
	}

	//clear(m)
	delete(m, "Crash")
	for _, v := range m {
		fmt.Println(v)
	}

}
