package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {

	cpus := runtime.NumCPU()
	fmt.Println(cpus)

	jobs := make(chan int)
	var wg sync.WaitGroup

	// wg.Add(cpus)
	for i := 0; i < cpus; i++ {
		//i := i
		wg.Add(1)
		go func(num int) {
			defer wg.Done()
			for job := range jobs {
				fmt.Printf("%d: %d\n", num, job)
			}
		}(i)
	}

	for i := 0; i < 10_000; i++ {
		jobs <- i
	}
	close(jobs)

	wg.Wait()
}
